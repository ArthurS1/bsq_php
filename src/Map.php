<?php

namespace BSQ;

class Map
{
    private $char_map; // stores the map in characters
    private $nb_map; // stores the underlying integer map we need for the algorithm

    public function __construct() {
        global $argv;

        $this->char_map = (array) null;
        $this->nb_map = (array) null;

        $fp = fopen($argv[1], "r");
        if (!$fp)
            throw new \Exception("file \"" . $argv[1] . "\" could not be opened.");
        $this->createCharMapFromFile($fp);
        fclose($fp);
    }

    // MAP CREATION ERROR HANDLING
    // checking the declared number of lines
    private function checkLineNbErrors($declared_line_nb, $map)
    {
        $declared_line_nb = substr($declared_line_nb, 0, strlen($declared_line_nb) - 1);

        if (!is_numeric($declared_line_nb))
            throw new \Exception("\"".$declared_line_nb."\" should be a numeric value.");
        else if ($declared_line_nb != count($map))
            throw new \Exception("wrong number of lines declared at the start of the file.");
    }

    // checking line length
    private function checkLineErrors($map)
    {
        $line_length = strlen($map[0]);

        if ($line_length == 0)
            exit(84);
        foreach ($map as $line) {
            if (strlen($line) != $line_length) {
                throw new \Exception("not all lines are the same length.");
            }
        }
    }

    // checking map content
    private function checkCharacterErrors($map)
    {
        foreach ($map as $line) {
            foreach (str_split($line) as $char) {
                if ($char != "o" and $char != ".") {
                    throw new \Exception("unexpected character in line \"" . $line . "\".");
                }
            }
        }
    }

    // MAP CREATION
    private function createCharMapFromFile($fp)
    {
        $temp_map = [];
        $declared_line_nb = fgets($fp);

        while (!feof($fp)) {
            $current_line = fgets($fp);
            $temp_map[] = substr($current_line, 0, strlen($current_line) - 1);
        }
        array_pop($temp_map);

        $this->checkLineNbErrors($declared_line_nb, $temp_map);
        $this->checkLineErrors($temp_map);
        $this->checkCharacterErrors($temp_map);

        foreach ($temp_map as $line) {
            $this->char_map[] = str_split($line);
        }
    }

    private function createNbMapFromCharMap()
    {
        $temp_line = [];

        foreach ($this->char_map as $line) {
            foreach ($line as $char) {
                if ($char == '.')
                    $temp_line[] = 1;
                else if ($char == 'o')
                    $temp_line[] = 0;
            }
            $this->nb_map[] = $temp_line;
            $temp_line = [];
        }
    }

    // PRINTING FUNCTIONS
    // prints the character map
    public function printChar()
    {
        foreach ($this->char_map as $line) {
            foreach ($line as $char)
                fwrite(STDOUT, $char);
            fwrite(STDOUT, "\n");
        }
    }

    // prints the underlying number map
    public function printNb()
    {
        foreach ($this->nb_map as $line) {
            foreach ($line as $nb)
                fwrite(STDOUT, $nb);
            fwrite(STDOUT, "\n");
        }
    }

    // SOLVING ALGORITHM
    // returns the lowest value of the three top left cells next to the current one
    private function getLowestValue($y, $x)
    {
        if ($this->nb_map[$y][$x] == 0) {
            return (-1);
        } else if ($x <= 0 or $y <= 0) {
            return (0);
        } else {
            $values = array(
                $this->nb_map[$y - 1][$x],
                $this->nb_map[$y - 1][$x - 1],
                $this->nb_map[$y][$x - 1]);

            arsort($values);
            return (array_pop($values));
        }
    }

    private function putSquare($coord) {
        for ($y = 0; $y < count($this->nb_map); $y++) {
            for ($x = 0; $x < count($this->nb_map[0]); $x++) {
                if ($y <= $coord[0] and $y >= $coord[0] - $coord[2]
                    and $x <= $coord[1] and $y >= $coord[1] - $coord[2])
                    $this->char_map[$y][$x] = "x";
            }
        }
    }

    private function getCoordBiggestSquare() {
        $tmp_coord = array(0, 0, 0); // in the form of (posY, posX, size of the square)

        for ($y = 0; $y < count($this->nb_map); $y++) {
            for ($x = 0; $x < count($this->nb_map[0]); $x++) {
                if ($this->nb_map[$y][$x] > $tmp_coord[2])
                    $tmp_coord = array($y, $x, $this->nb_map[$y][$x]);
            }
        }
        return ($tmp_coord);
    }

    // modifies the character map with the solution
    public function solveReversedMinesweeper()
    {
        $this->createNbMapFromCharMap();

        for ($y = 0; $y < count($this->nb_map); $y++) {
            for ($x = 0; $x < count($this->nb_map[0]); $x++) {
                $this->nb_map[$y][$x] = $this->getLowestValue($y, $x) + 1;
            }
        }
        $this->putSquare($this->getCoordBiggestSquare());
    }

}
