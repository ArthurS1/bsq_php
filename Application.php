#!/bin/php
<?php

use BSQ\Map as Map;

const FAILURE = 84;

// autoloading applying PSR-4 test (functional)
spl_autoload_register(function ($class) {
    $prefix = 'BSQ\\';
    $base_dir = __DIR__ . '/src/';
    $len = strlen($prefix);

    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

// checking CLI arguments
if (count($argv) != 2) {
    fwrite(STDERR, "Error while parsing arguments.\n");
    exit(FAILURE);
}

try {
    $map = new Map();
} catch (Exception $e) {
    fwrite(STDERR, "Error while creating the map: " . $e->getMessage() . "\n");
    exit(FAILURE);
}

$map->solveReversedMinesweeper();
$map->printChar();
